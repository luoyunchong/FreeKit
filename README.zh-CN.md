# IGeekFan.FreeKit


<div align="center">
<h1 align="center"> <img alt="logo" src="docs/images/logo.png" width="40px" />  IGeekFan.FreeKit </h1>

**Freekit** 为.NET Core提供了更多的扩展实现

[![.NET IDE Rider](https://img.shields.io/static/v1?style=float&logo=rider&label=Rider&message=jetbrains&color=red)](https://www.jetbrains.com/rider/)
[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://raw.githubusercontent.com/luoyunchong/IGeekFan.AspNetCore.RapiDoc/master/LICENSE)
<p>
    <span>中文</span> |  
    <a href="README.md">English</a>
</p>
</div>


# TODO